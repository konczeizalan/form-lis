%{
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "listaz.tab.h"

FILE *g;
%}

%option noyywrap

__COMM ("/*"[^*]*\*([*]*[^*/][^*]*\*)*\/)

/* start */
__CBREKB ("... _ ._ ._. _")
/* finish */
__CBREKE (".._. .. _. .. ... ....")
/* cstart */
__BREKB ("_._. ... _ ._ ._. _")
/* cfinish */
__BREKE ("._._ .._. .. _. .. ... ....")

__NLINE ("::__::")

__AND ("_:__:_")
__OR  (".:..:.")

__INC (":.::.:")
__DEC (":_::_:")
__MOD (":_.:")
__MUL (":..:")
__DIV (":__:")
__ADD (":.:")
__SUB (":_:")

__COMMA (".::.")
__END (":..")

__NEGEQ ("_::")
__LE (".::")
__GE ("::.")
__EQ ("::")
__LT (".:")
__GT (":.")

__NEG ("_:")

__AS (":")

___1 (".____")
___2 ("..___")
___3 ("...__")
___4 ("...._")
___5 (".....")
___6 ("_....")
___7 ("__...")
___8 ("___..")
___9 ("____.")
___0 ("_____")

__H ("....")
__V ("..._")
__F (".._.")
__L ("._..")
__P (".__.")
__J (".___")
__B ("_...")
__X ("_.._")
__C ("_._.")
__Y ("_.__")
__Z ("__..")
__Q ("__._")

__S ("...")
__U (".._")
__R ("._.")
__W (".__")
__D ("_..")
__K ("_._")
__G ("__.")
__O ("___")

__I ("..")
__A ("._")
__N ("_.")
__M ("__")

__E (".")
__T ("_")

%%

{__COMM} {fprintf(g, "%s", zztext);}

{__BREKB} {fprintf(g, "(");}
{__BREKE} {fprintf(g, ")");}
{__CBREKB} {fprintf(g, "{");}
{__CBREKE} {fprintf(g, "}");}

{__NLINE} {fprintf(g, "\n");}

{__AND} {fprintf(g, "&&");}
{__OR} {fprintf(g, "||");}

{__INC} {fprintf(g, "++");}
{__DEC} {fprintf(g, "--");}
{__MOD} {fprintf(g, "%");}
{__MUL} {fprintf(g, "*");}
{__DIV} {fprintf(g, "/");}
{__ADD} {fprintf(g, "+");}
{__SUB} {fprintf(g, "-");}

{__COMMA} {fprintf(g, ",");}
{__END} {fprintf(g, ";");}

{__NEGEQ} {fprintf(g, "!=");}
{__LE} {fprintf(g, "<=");}
{__GE} {fprintf(g, ">=");}
{__EQ} {fprintf(g, "==");}
{__LT} {fprintf(g, "<");}
{__GT} {fprintf(g, ">");}

{__NEG} {fprintf(g, "!");}

{__AS} {fprintf(g, "=");}

{___1} {fprintf(g, "1");}
{___2} {fprintf(g, "2");}
{___3} {fprintf(g, "3");}
{___4} {fprintf(g, "4");}
{___5} {fprintf(g, "5");}
{___6} {fprintf(g, "6");}
{___7} {fprintf(g, "7");}
{___8} {fprintf(g, "8");}
{___9} {fprintf(g, "9");}
{___0} {fprintf(g, "0");}

{__H} {fprintf(g, "h");}
{__V} {fprintf(g, "v");}
{__F} {fprintf(g, "f");}
{__L} {fprintf(g, "l");}
{__P} {fprintf(g, "p");}
{__J} {fprintf(g, "j");}
{__B} {fprintf(g, "b");}
{__X} {fprintf(g, "x");}
{__C} {fprintf(g, "c");}
{__Y} {fprintf(g, "y");}
{__Z} {fprintf(g, "z");}
{__Q} {fprintf(g, "q");}

{__S} {fprintf(g, "s");}
{__U} {fprintf(g, "u");}
{__R} {fprintf(g, "r");}
{__W} {fprintf(g, "w");}
{__D} {fprintf(g, "d");}
{__K} {fprintf(g, "k");}
{__G} {fprintf(g, "g");}
{__O} {fprintf(g, "o");}

{__I} {fprintf(g, "i");}
{__A} {fprintf(g, "a");}
{__N} {fprintf(g, "n");}
{__M} {fprintf(g, "m");}

{__E} {fprintf(g, "e");}
{__T} {fprintf(g, "t");}

" " {}
"\t" {fprintf(g, " ");}
"\n" {fprintf(g, "\n");}

. {fprintf(g, "%s", zztext);}

%%

void compiler() {
	printf("comp begining\n");
	g = fopen ("forditott.c","w");
	yylex();
	fclose(yyin);
	fclose(g);
}
