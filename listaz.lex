%{
#include <iostream>
#include <fstream>
#include "listaz.tab.h"
int column=0;
FILE *f;

%}

%option noyywrap
%option yylineno


/*valltozok*/

VAR (("."|"_"){1,4}" ")+

/*operatorok*/

/*AROP (\+|-|\+\+|--|\*|\/)*/
AROP (":.:"|":_:"|":..:"|":__:")
INC (":.::.:")
DEC (":_::_:")
MOD (":_.:")
/*RELOP (==|!=|>|<|<=|>=)*/
RELOP ("::"|"_::"|".:"|":."|::.""|".::")
/*LOGOP (\|\||&&)*/
LOGOP (".:..:."|"_:__:_")
/*NEG (!)*/
NEG ("_:")
/*BITOP (&|^|\||~|<<|>>) {BITOP} {fprintf(f,"%d %d %d %s Bit operator\n",yylineno,column,yyleng,yytext );column+=yyleng;}*/
/*ASOP (=|\+=|-=|\*=|\/=|%=|<<=|>>=|&=|^=|\|=)*/
EQUAL (":")

/*keywords*/

/*LOOP (while|for|do)*/
WHILE (".__ .... .. ._.. .")
FOR (".._. ___ ._.")
DO ("_.. ___")
/*LOGIC (if|else|swich|case)*/
IF (".. .._.")
ELSE (". ._.. ... .")
SWICH ("... .__ .. _ _._. ....")
CASE ("_._. ._ ... .")
/**/
VOID ("..._ ___ .. _..")
INT (".. _. _")
DOUBLE ("_.. ___ .._ _... ._.. .")
CHAR ("_._. .... _. ._.")
STRING "((("."|"_"){1,4}" ")*)"\t"?"
/*CONTROL (break|continue)*/
CONTROL ("_... ._. . ._ _._"|"_._. ___ _. _ .. _. .._ .")
/*CONSTANS (const)*/
CONSTANS ("_._. ___ _. ... _")
/*RETURN (return)*/
RETURN ("._. . _ .._ ._. _.")
/*READ ("cin>>")*/
READ ("._._ .. _. :. :.")
/*WRITE ("cout<<")*/
WRITE ("._._ .._ ___ _ .: .:")
/*NUMBER*/

INTNUM ("_____"|".____"|"..___"|"...__"|"...._"|"....."|"_...."|"__..."|"___.."|"____.")+
RNUM ("_____"|".____"|"..___"|"...__"|"...._"|"....."|"_...."|"__..."|"___.."|"____.")+,("_____"|".____"|"..___"|"...__"|"...._"|"....."|"_...."|"__..."|"___.."|"____.")+

/*braket*/

/*BREK (\(|\)|\[|\]|\{|\})*/
/*BREK (start|finis)*/
/*CBREK (|cstart|cfinish)*/
BREKB ("... _ ._ ._. _")
BREKE (".._. .. _. .. ... ....")
CBREKB ("_._. ... _ ._ ._. _")
CBREKE ("._._ .._. .. _. .. ... ....")

/*COMMA (,)*/
COMMA (".::.")

/*end command*/

/*END (;)*/
END (":..")

NLINE ("::__::")

/* comment */

COMM ("/*"[^*]*\*([*]*[^*/][^*]*\*)*\/)

/*{COMM} {fprintf(f,"%d %d %d %s comment\n",yylineno,column,yyleng,yytext );column+=yyleng;}*/
%%

{WHILE} {fprintf(f,"%d %d %d %s while\n",yylineno,column,yyleng,yytext ); column+=yyleng;return WHILE;}
{FOR} {fprintf(f,"%d %d %d %s for\n",yylineno,column,yyleng,yytext ); column+=yyleng;return FOR;}
{DO} {fprintf(f,"%d %d %d %s do\n",yylineno,column,yyleng,yytext ); column+=yyleng;return DO;}
{IF} {fprintf(f,"%d %d %d %s logical if\n",yylineno,column,yyleng,yytext ); column+=yyleng;return IF;}
{ELSE} {fprintf(f,"%d %d %d %s logical else\n",yylineno,column,yyleng,yytext ); column+=yyleng;return ELSE;}
{SWICH} {fprintf(f,"%d %d %d %s logical\n",yylineno,column,yyleng,yytext ); column+=yyleng;return SWICH;}
{CASE} {fprintf(f,"%d %d %d %s logical\n",yylineno,column,yyleng,yytext ); column+=yyleng;return CASE;}

{VOID} {fprintf(f,"%d %d %d %s type void\n",yylineno,column,yyleng,yytext );column+=yyleng;yylval.name = strdup(yytext);return VOID;}
{INT} {fprintf(f,"%d %d %d %s type int \n",yylineno,column,yyleng,yytext );column+=yyleng;yylval.name = strdup(yytext);return INT;}
{DOUBLE} {fprintf(f,"%d %d %d %s type double\n",yylineno,column,yyleng,yytext );column+=yyleng;yylval.name = strdup(yytext);return DOUBLE;}
{CHAR} {fprintf(f,"%d %d %d %s type char\n",yylineno,column,yyleng,yytext );column+=yyleng;yylval.name = strdup(yytext);return CHAR;}
{CONTROL} {fprintf(f,"%d %d %d %s Control\n",yylineno,column,yyleng,yytext );column+=yyleng;return CONTROL;}
{CONSTANS} {fprintf(f,"%d %d %d %s constans\n",yylineno,column,yyleng,yytext );column+=yyleng;return CONSTANS;}
{RETURN} {fprintf(f,"%d %d %d %s return\n",yylineno,column,yyleng,yytext );column+=yyleng;return RETURN;}
{READ} {fprintf(f,"%d %d %d %s read\n",yylineno,column,yyleng,yytext );column+=yyleng;return READ;}
{WRITE} {fprintf(f,"%d %d %d %s write\n",yylineno,column,yyleng,yytext );column+=yyleng;return WRITE;}

{AROP} {fprintf(f,"%d %d %d %s Aritmetic operator\n",yylineno,column,yyleng,yytext );column+=yyleng;return AROP;}
{INC} {fprintf(f,"%d %d %d %s Aritmetic operator int\n",yylineno,column,yyleng,yytext );column+=yyleng;return INC;}
{DEC} {fprintf(f,"%d %d %d %s Aritmetic operator dec\n",yylineno,column,yyleng,yytext );column+=yyleng;return DEC;}
{MOD} {fprintf(f,"%d %d %d %s Aritmetic operator mod\n",yylineno,column,yyleng,yytext );column+=yyleng;return MOD;}
{RELOP} {fprintf(f,"%d %d %d %s Ralational operator\n",yylineno,column,yyleng,yytext );column+=yyleng;return RELOP;}
{LOGOP} {fprintf(f,"%d %d %d %s Logical operator\n",yylineno,column,yyleng,yytext );column+=yyleng;return LOGIC;}
{NEG} {fprintf(f,"%d %d %d %s Neg operator\n",yylineno,column,yyleng,yytext );column+=yyleng;return NEG;}
{EQUAL} {fprintf(f,"%d %d %d %s equal operator\n",yylineno,column,yyleng,yytext );column+=yyleng;return EQUAL;}

{RNUM} {fprintf(f,"%d %d %d %s Real number\n",yylineno,column,yyleng,yytext );column+=yyleng;return RNUM;}
{INTNUM} {fprintf(f,"%d %d %d %s Int number\n",yylineno,column,yyleng,yytext );column+=yyleng;return INTNUM;}

{BREKB} {fprintf(f,"%d %d %d %s Brek begin\n",yylineno,column,yyleng,yytext );column+=yyleng;yylval.name = strdup(yytext);return BREKB;}
{BREKE} {fprintf(f,"%d %d %d %s Brek end\n",yylineno,column,yyleng,yytext );column+=yyleng;yylval.name = strdup(yytext);return BREKE;}
{CBREKB} {fprintf(f,"%d %d %d %s CBrek begin\n",yylineno,column,yyleng,yytext );column+=yyleng;yylval.name = strdup(yytext);return CBREKB;}
{CBREKE} {fprintf(f,"%d %d %d %s CBrek end\n",yylineno,column,yyleng,yytext );column+=yyleng;yylval.name = strdup(yytext);return CBREKE;}
{END} {fprintf(f,"%d %d %d %s End\n",yylineno,column,yyleng,yytext );column+=yyleng;yylval.name = strdup(yytext);return END;}

{VAR}/\( {fprintf(f,"%d %d %d %s function\n",yylineno,column,yyleng,yytext );column+=yyleng; yylval.name = strdup(yytext);return FUNC;}
{VAR} {fprintf(f,"%d %d %d %s variable\n",yylineno,column,yyleng,yytext );column+=yyleng; yylval.name = strdup(yytext); return VAR;}

{COMM} {fprintf(f,"%d %d %d %s comment\n",yylineno,column,yyleng,yytext );column+=yyleng;return COMM;}
{COMMA} {fprintf(f,"%d %d %d %s comma\n",yylineno,column,yyleng,yytext );column+=yyleng;return COMMA;}

\n {column=0;}
(\t|" ") {column++;}
. {fprintf(f,"%d %d %d %s sada\n",yylineno,column,yyleng,yytext );column++;}

%%

//  int main(int argc, char *argv[]) {
//      yyin = fopen (argv[1],"r");
//     f = fopen (argv[2],"w");
//     yylex();
//     fclose(yyin);
//     fclose(f);
// }
