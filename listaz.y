%{
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <map>

using namespace std;

extern int yylex();
extern int yylineno;
extern int column;
extern void compiler();
int VarHatokor(string s,int h);
int hatokor = 0;
map<string,string> valtozok[25];
int yyerror(const char*);
int yyerrok();
%}

%union {
	char* value;
	char* name;
}

%error-verbose

%start commands

%token INTNUM RNUM
%token RETURN
%token <name> VAR
%token END
%token CONSTANS
%token LOOP LOGIC TYPE CONTROL 
%token BREK
%token FUNC COMM
%token EQUAL COMMA NEG
%token IF ELSE WHILE FOR DO SWICH CASE
%token <name> CHAR DOUBLE STRING INT VOID
%token BREKB BREKE CBREKB CBREKE
%token AROP
%token READ WRITE

%left EQUALS
%left LOGOP
%left RELOP
%left AROP MOD
%left INC DEC

%%

reader:READ VAR END { if(VarHatokor($2,hatokor) !=1 ) yyerror("Nem volt deklaralva"); }
;
writer:WRITE VAR END { if(VarHatokor($2,hatokor) !=1 ) yyerror("Nem volt deklaralva"); }
 |WRITE INTNUM END
 |WRITE RNUM END
 |WRITE simple_op END
 |WRITE STRING END
;
expr:VAR { if(VarHatokor($1,hatokor) !=1 ) yyerror("Nem volt deklaralva "); }
 |INTNUM
 |RNUM
 |CONSTANS
 |expr AROP expr
;
small_logic_expr:expr
 |expr RELOP expr {printf("Logikai kifejezes\n");}
;
logic_expr: small_logic_expr
 |small_logic_expr LOGOP small_logic_expr
 |NEG small_logic_expr {printf("Logikai kifejezes\n");}
;
arg:VAR { if(VarHatokor($1,hatokor) !=1 ) yyerror("Nem volt deklaralva"); }
 |INTNUM
 |RNUM
 |STRING
;
args:arg
 |args COMMA arg
;
declaration: VOID VAR END { if(valtozok[hatokor].find($2) != valtozok[hatokor].end()) yyerror("Mar egyszer volt deklaralva"); else valtozok[hatokor].insert(std::pair<string,string>($2,$1));}
 |INT VAR END {if(valtozok[hatokor].find($2) != valtozok[hatokor].end()) yyerror("Mar egyszer volt deklaralva"); else valtozok[hatokor].insert(std::pair<string,string>($2,$1));}
 |DOUBLE VAR END {if(valtozok[hatokor].find($2) != valtozok[hatokor].end()) yyerror("Mar egyszer volt deklaralva"); else valtozok[hatokor].insert(std::pair<string,string>($2,$1));}
 |CHAR VAR END{if(valtozok[hatokor].find($2) != valtozok[hatokor].end()) yyerror("Mar egyszer volt deklaralva"); else valtozok[hatokor].insert(std::pair<string,string>($2,$1));}
 |INT VAR EQUAL INTNUM END {if(valtozok[hatokor].find($2) != valtozok[hatokor].end()) yyerror("Mar egyszer volt deklaralva"); else valtozok[hatokor].insert(std::pair<string,string>($2,$1));}
 |DOUBLE VAR EQUAL DOUBLE END {if(valtozok[hatokor].find($2) != valtozok[hatokor].end()) yyerror("Mar egyszer volt deklaralva"); else valtozok[hatokor].insert(std::pair<string,string>($2,$1));}
 |CHAR VAR EQUAL CHAR END {if(valtozok[hatokor].find($2) != valtozok[hatokor].end()) yyerror("Mar egyszer volt deklaralva"); else valtozok[hatokor].insert(std::pair<string,string>($2,$1));}
;
operation: VAR EQUAL VAR AROP VAR { if(VarHatokor($1,hatokor) != 1 || VarHatokor($3,hatokor) != 1 || VarHatokor($5,hatokor) != 1) yyerror("Nem volt deklaralva"); else if(valtozok[hatokor].find($1)->second != valtozok[hatokor].find($3)->second || valtozok[hatokor].find($3)->second != valtozok[hatokor].find($1)->second) yyerror("Rossz tipus");}
 |VAR AROP VAR { if(VarHatokor($1,hatokor) != 1 || VarHatokor($3,hatokor) != 1) {yyerror("Nem volt deklaralva");} else {if(valtozok[hatokor].find($1)->second != valtozok[hatokor].find($3)->second) yyerror("Rossz tipus");}}
 |VAR INC { if(VarHatokor($1,hatokor) != 1 ) yyerror("Nem volt deklaralva"); }
 |VAR DEC { if(VarHatokor($1,hatokor) != 1 ) yyerror("Nem volt deklaralva"); }
;
simple_op:operation END
;
for_op:operation CBREKE
;
type:INT
 |DOUBLE
 |CHAR
 |VOID
;
func_arg:type VAR { if(VarHatokor($2,hatokor) !=1 ) yyerror("Nem volt deklaralva"); }
;
func_args:func_arg
 |func_args COMMA func_arg
;
function:type FUNC CBREKB func_args CBREKE block
;
command:declaration {printf("Deklaracio\n");}
 |COMM {printf("Komment\n");}
 |loop
 |RETURN VAR END {printf("return\n");} { if(valtozok[hatokor].find($2) == valtozok[hatokor].end()) cout<<"Meg nem volt deklaralva "<<hatokor;}
 |function {printf("Függvény deklaráció\n");}
 |FUNC CBREKB args CBREKE END {printf("Függvény hívás\n");}
 |cross_road
 |simple_op {printf("Muvelet\n");}
 |reader {printf("Read\n");}
 |writer {printf("Write\n");}
;
commands:command
 |commands command
;
brekb: BREKB {hatokor++;}
;
breke: BREKE {hatokor--;}
;
block:brekb commands breke 
;
case:CASE expr CBREKE commands
;
loop:WHILE CBREKB logic_expr CBREKE block {printf("While ciklus\n");}
 |DO block WHILE CBREKB logic_expr CBREKE {printf("Do-while ciklus\n");}
 |FOR CBREKB command logic_expr END for_op block {printf("For ciklus\n");}
;
case_block:case
 |case_block case
;
cross_road:IF CBREKB logic_expr CBREKE block {printf("if(...){...}\n");}
 |IF CBREKB logic_expr CBREKE block ELSE block {printf("if(...){...}else{...}\n");}
 |SWICH CBREKB expr CBREKE BREKB case_block BREKE {printf("swich(...){case ...: ...}");}
;

%%

int main() {
	if (yyparse()==0)
		printf("\nOK\n");
  return 0;
}

int yyerror(const char* s) {
    printf("%s, Line: %d, Column: %d \n",s,yylineno,column);
    return -1;
}

int VarHatokor(string s,int h)
{
	int i=h;
	int volt = 0;
	while (i>=0)
	{
		 if(valtozok[i].find(s) != valtozok[i].end()) volt=1;
		 i--;
	}
	return volt;
}
