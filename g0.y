%{
#include <iostream>
using namespace std;

extern int yylex();
int yyerror(const char*);
%}

%%

s: 'a' s 'b'
 | 'a'
; 

%%

int main() {
	if (yyparse() == 0)
		cout << "OK" << endl;
}

int yyerror(const char* s) {
	cout << s << endl;
}
