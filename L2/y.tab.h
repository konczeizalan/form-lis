
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     PLUS = 258,
     MINUS = 259,
     TIMES = 260,
     DIVIDED = 261,
     IS = 262,
     EQUALS = 263,
     NOTEQUALS = 264,
     NOT = 265,
     AND = 266,
     OR = 267,
     BAND = 268,
     BOR = 269,
     BXOR = 270,
     INT = 271,
     FLOAT = 272,
     IF = 273,
     WHILE = 274,
     PRINT = 275,
     READ = 276,
     INTNUM = 277,
     FLOATNUM = 278,
     VARIABLE = 279,
     SBEGIN = 280,
     SEND = 281,
     RBEGIN = 282,
     REND = 283,
     CBEGIN = 284,
     CEND = 285,
     SC = 286
   };
#endif
/* Tokens.  */
#define PLUS 258
#define MINUS 259
#define TIMES 260
#define DIVIDED 261
#define IS 262
#define EQUALS 263
#define NOTEQUALS 264
#define NOT 265
#define AND 266
#define OR 267
#define BAND 268
#define BOR 269
#define BXOR 270
#define INT 271
#define FLOAT 272
#define IF 273
#define WHILE 274
#define PRINT 275
#define READ 276
#define INTNUM 277
#define FLOATNUM 278
#define VARIABLE 279
#define SBEGIN 280
#define SEND 281
#define RBEGIN 282
#define REND 283
#define CBEGIN 284
#define CEND 285
#define SC 286




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;


