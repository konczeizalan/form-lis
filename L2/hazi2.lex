%{
/*#include <iostream>
using namespace std;*/
#include <stdio.h>
#include <stdlib.h>
#include "y.tab.h"
%}

%option noyywrap
%option yylineno

INTEGER ([1-9][0-9]*)|0
FLOAT (([1-9][0-9]*)|0)\.[0-9]+
VARIABLE [a-zA-Z][a-zA-Z0-9]*


%%

"plus" { 	return PLUS;	}
"minus" { 	return MINUS;	}
"times" { 	return TIMES;	}
"divided" { 	return DIVIDED;	}

"is" { 	return IS;	}

"equals" { return EQUALS; }

"notequals" { 	return NOTEQUALS;	}

"not" { return NOT;	}

"and" { return AND;	}

"band" {return BAND; }

"or" { return OR;	}

"bor" { return BOR;          }

"bxor" { return BXOR; }

"int" { return INT;	}

"float" {return FLOAT; }

"if" { return IF; }

"while" { return WHILE; }

"print" { return PRINT; }

"read" { return READ; }

"[" {return SBEGIN;}

"]" {return SEND;}

"(" {return RBEGIN;}

")" {return REND;}

"{" {return CBEGIN;}

"}" {return CEND;}

";" {return SC;}

{INTEGER} { return INTNUM; }

{FLOAT} { return FLOATNUM; }

{VARIABLE} { return VARIABLE;	}


"\t" { 	}

" " {	}

"\n" {	}

"//".* { }

. { return yytext[0]; }

%%
