%{
/*#include <iostream>*/
#include <stdio.h>
#include <stdlib.h>
extern int yylex();
extern int yylineno;
int yyerror(const char*);
int yyerrok();
%}

%error-verbose

%token PLUS MINUS TIMES DIVIDED
%token IS
%token EQUALS NOTEQUALS
%token NOT AND OR
%token BAND BOR BXOR
%token INT FLOAT
%token IF WHILE
%token PRINT READ
%token INTNUM FLOATNUM
%token VARIABLE
%token SBEGIN SEND
%token RBEGIN REND
%token CBEGIN CEND
%token SC


%left EQUALS NOTEQUALS
%left AND OR
%left BAND BOR BXOR
%left PLUS MINUS TIMES DIVIDED
%right NOT IS

%%

e: exp
 | e exp

exp:VARIABLE IS nexp SC {printf(" ertekadas\n");}
  | INT VARIABLE SC {printf(" bejelentes\n");}
  | INT VARIABLE IS nexp SC {printf(" bejelentes ertekadas\n");}
  | FLOAT VARIABLE SC {printf(" float bejelentes \n");}
  | FLOAT VARIABLE IS nexp SC {printf(" float bejelentes ertekadas\n");}
  | INT VARIABLE SBEGIN nexp SEND SC {printf(" Array bejelentes\n");}
  | INT VARIABLE SBEGIN nexp SEND IS CBEGIN arr CEND SC { printf(" array bejelentes ertekadas\n");}
  | WHILE bexp CBEGIN e CEND {printf(" while \n");}
  | IF bexp CBEGIN e CEND {printf(" if \n");}
  | PRINT VARIABLE SC {printf(" kiiras \n");}
  | READ VARIABLE SC {printf(" beolvasas \n");}
;

arr: nexp
 | arr nexp

bexp: nexp EQUALS nexp
	| nexp NOTEQUALS nexp
  | RBEGIN bexp REND
  | bexp OR bexp
  | bexp AND bexp
  | NOT RBEGIN bexp REND
;


nexp: INTNUM     
   | FLOATNUM       
   | VARIABLE      
   | nexp TIMES nexp { printf(" szor \n"); }
   | nexp MINUS nexp { printf(" minnusz \n"); }
   | nexp PLUS nexp { printf(" plusz \n"); }
   | nexp DIVIDED nexp { printf(" oszt \n"); }
   | nexp BAND nexp { printf(" es \n");}
   | nexp BOR nexp {printf(" vagy \n");}
   | nexp BXOR nexp {printf(" kizaro vagy \n");}
   | RBEGIN nexp REND {printf(" zarojelbe szam \n");}
;

%%


int main() {
	if (yyparse()==0)
		printf("\nOK\n");
  return 0;
}

int yyerror(const char* s) {
    printf("%s, \t%d\n",s,yylineno);
    return -1;
}
